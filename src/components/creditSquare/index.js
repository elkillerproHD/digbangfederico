import React, { useState } from 'react'
import styled from 'styled-components'
import Slider from 'rc-slider'
import 'rc-slider/assets/index.css'
import './sliderBoxShadowOverwrite.css'

const strings = {
  title: 'Simulá tu credito',
  totalMount: 'MONTO TOTAL',
  period: 'PLAZO',
  monthlyPrice: 'CUOTA FIJA POR MES',
  getCredit: 'OBTENÉ CRÉDITO',
  seeDetails: 'VER DETALLE DE CUOTAS',
  marksSlider: {
    priceStart: '$ 5.000',
    priceEnd: '$ 50.000',
  },
}

const Square = styled.div`
  width: 450px;
  height: 500px;
  background: #003b67;
`
const Title = styled.h1`
  font-family: 'Montserrat';
  font-weight: bold;
  text-align: center;
  width: 100%;
  color: white;
`
const SliderContainer = styled.div`
  width: 70%;
  padding-left: 15%;
  padding-right: 15%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`
const PriceMark = styled.p`
  font-family: 'Montserrat';
  font-weight: 500;
  font-size: 14px;
  color: #fff;
  bottom: -20px;
  position: absolute;
`
const LabelContainer = styled.div`
  width: 82%;
  padding-left: 9%;
  padding-right: 9%;
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const LabelText = styled.p`
  font-family: 'Montserrat';
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`
const LabelValue = styled.input`
  border: 1px solid #fff;
  background-color: transparent;
  padding: 0 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  height: 25px;
  width: 78px;
  font-family: 'Montserrat';
  font-weight: 700;
  font-size: 17px;
  color: #fff;
  outline: none;
`

const EmptySpace = styled.div`
  width: 100%;
  height: 38px;
`

const TotalQuotesContainer = styled.div`
  width: 75%;
  padding-left: 5%;
  padding-right: 5%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-left: 7.5%;
  height: 50px;
  margin-top: 40px;
  background-color: #00355d;
`
const GetCreditButton = styled.div`
  background-color: #17aa8d;
  width: 68%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  height: 50px;
  &:hover {
    opacity: 0.9;
  }
`
const GetDetailsButton = styled.div`
  background-color: #0b548b;
  width: 30%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  height: 50px;
  &:hover {
    opacity: 0.9;
  }
`
const GetCreditGetDetailsContainer = styled.div`
  width: 85%;
  height: 50px;
  margin-left: 7.5%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const BoldText = styled.p`
  font-family: 'Montserrat';
  font-weight: 700;
  color: #fff;
`

function NumberWithDots(x, canBeFloat = false) {
  if (canBeFloat) {
    x = x.toFixed(2)
  }
  x = x.toString()
  const pattern = /(-?\d+)(\d{3})/
  while (pattern.test(x)) {
    x = x.replace(pattern, '$1&$2')
  }
  x = x.replace('.', ',')
  x = x.replace('&', '.')
  return x
}

export const CreditSquare = props => {
  const [price, setPrice] = useState(19500)
  const [inputPrice, setInputPrice] = useState(19500)
  const [quotes, setQuotes] = useState(12)
  const [inputQuotes, setInputQuotes] = useState(12)
  const [quotePrice, setQuotePrice] = useState(19500 / 12)
  const OnChangeInputPriceValue = value => {
    value = value.replace('.', '')
    value = value.replace('$', '')
    while (value.search(' ') !== -1) {
      value = value.replace(' ', '')
    }
    if (parseInt(value) === 0) {
      setInputPrice(0)
      return
    }
    if (value.length > 5) {
      return
    }
    if (value.length < 1) {
      setInputPrice('')
      return
    }
    setInputPrice(parseInt(value))
  }
  const BlurPriceInput = () => {
    let value = inputPrice.toString()
    value = value.replace('.', '')
    value = value.replace('$', '')
    while (value.search(' ') !== -1) {
      value = value.replace(' ', '')
    }
    if (value.length < 1) {
      setInputPrice(5000)
      setPrice(5000)
      return
    }
    if (value < 5000) {
      setInputPrice(5000)
      setPrice(5000)
    } else if (value > 50000) {
      setInputPrice(50000)
      setPrice(50000)
    }
    setPrice(value)
  }
  const BlurQuoteInput = () => {
    if (quotes > 24) {
      setQuotes(24)
      setInputQuotes(24)
    } else if (quotes < 3) {
      setQuotes(3)
      setInputQuotes(3)
    } else {
      setQuotes(inputQuotes)
    }
  }
  return (
    <Square>
      <Title>{strings.title}</Title>
      <LabelContainer>
        <LabelText>{strings.totalMount}</LabelText>
        <LabelValue
          onChange={e => OnChangeInputPriceValue(e.target.value)}
          onBlur={() => BlurPriceInput()}
          value={`$ ${NumberWithDots(inputPrice)}`}
        />
      </LabelContainer>
      <SliderContainer>
        <Slider
          min={5000}
          max={50000}
          value={price}
          railStyle={{ backgroundColor: 'white', borderRadius: 0 }}
          trackStyle={{ backgroundColor: 'white' }}
          handleStyle={{
            borderColor: 'white',
            backgroundColor: 'white',
          }}
          onChange={e => {
            setPrice(e)
            setInputPrice(e)
            setQuotePrice(e / quotes)
          }}
        />
        <PriceMark style={{ left: '40px' }}>{strings.marksSlider.priceStart}</PriceMark>
        <PriceMark style={{ right: '45px' }}>{strings.marksSlider.priceEnd}</PriceMark>
      </SliderContainer>
      <EmptySpace />
      <LabelContainer>
        <LabelText>{strings.period}</LabelText>
        <LabelValue
          value={inputQuotes}
          onBlur={() => BlurQuoteInput()}
          onChange={e => setInputQuotes(e.target.value)}
        />
      </LabelContainer>
      <SliderContainer>
        <Slider
          min={3}
          max={24}
          value={quotes}
          railStyle={{ backgroundColor: 'white', borderRadius: 0 }}
          trackStyle={{ backgroundColor: 'white' }}
          handleStyle={{
            borderColor: 'white',
            backgroundColor: 'white',
          }}
          onChange={e => {
            setQuotes(e)
            setInputQuotes(e)
            setQuotePrice(price / e)
          }}
        />
        <PriceMark style={{ left: '68px' }}>{3}</PriceMark>
        <PriceMark style={{ right: '68px' }}>{24}</PriceMark>
      </SliderContainer>
      <TotalQuotesContainer>
        <BoldText style={{ fontSize: 13 }}>{strings.monthlyPrice}</BoldText>
        <BoldText style={{ fontSize: 20 }}>{`$ ${NumberWithDots(quotePrice, true)}`}</BoldText>
      </TotalQuotesContainer>
      <GetCreditGetDetailsContainer>
        <GetCreditButton>
          <BoldText style={{ fontSize: 16 }}>{strings.getCredit}</BoldText>
        </GetCreditButton>
        <GetDetailsButton>
          <BoldText style={{ fontSize: 13, textAlign: 'center' }}>{strings.seeDetails}</BoldText>
        </GetDetailsButton>
      </GetCreditGetDetailsContainer>
    </Square>
  )
}
