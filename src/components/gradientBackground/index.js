import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  background: linear-gradient(180deg, rgba(8,79,133,1) 35%, rgba(0,59,103,1) 85%);
  width: 550px;
  height: 600px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const GradientBackground = props => {
  return <Container>{props.children}</Container>
}
