import React from 'react'
import styled from "styled-components";
import { GradientBackground } from './components/gradientBackground'
import { CreditSquare } from './components/creditSquare'

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`

function App() {
  return (
    <Container>
      <GradientBackground>
        <CreditSquare />
      </GradientBackground>
    </Container>
  )
}

export default App
